import pythoncom, pyHook
import timer, time, os
from threading import Timer

counts = {}

def on_keydown(event):
        key = event.GetKey()
        if key not in counts:
                counts[key] = 1
        else:
                counts[key] += 1

def writecounts():
        while true:
                file = open(os.path.expanduser('~') + "\count.txt")

                for key in counts:
                        if key.startswith("Media_"):
                                key = key[6:]
                        print key + "\t" + str(counts[key]), file

                file.close()
                time.sleep(60)

thread = Timer(60.0, writecounts)
thread.start()

hook = pyHook.HookManager()
hook.KeyDown = on_keydown
hook.HookKeyboard()
pythoncom.PumpMessages()